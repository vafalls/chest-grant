import Vue from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

var VueCookie = require('vue-cookie');

Vue.use(VueCookie);
Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.directive('focus', {
  inserted: function (el) {
    // Focus the element
    el.focus()
  }
});

new Vue({
  render: h => h(App),
}).$mount('#app');
